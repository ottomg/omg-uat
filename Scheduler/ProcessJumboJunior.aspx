﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProcessJumboJunior.aspx.vb" Inherits="ProcessJumboJunior" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="Button1" runat="server" Text="Test OMG Process Jumbo Junior" />
    
        <br />
        <br />
        <table class="auto-style1">
            <tr>
                <td style="text-align: center; width: 50%;">Request</td>
                <td style="text-align: center">Response</td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralRequest" runat="server"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="LiteralResponse" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
