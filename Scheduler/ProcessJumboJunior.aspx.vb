﻿Imports System
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization

Partial Class ProcessJumboJunior
    Inherits System.Web.UI.Page

    Dim MyFunction As New CDR_Function
    Public WS_HEIS As New HEIS.HEISService
    Dim MyDb As New dbConn
    Dim strConn As New SqlConnection(MyDb.connString)

    Dim TotalRecord As Integer = 0

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try
            If strConn.State = ConnectionState.Closed Then
                strConn.Open()
            End If

            Dim Status As Integer = 9
            Dim ProcessStatus As String = "Successful Add JJ product"

            Dim DeveloperId As String = "Adm1N"
            Dim DeveloperPassword As String = "Dev4pp2016"
            Dim b4hashstringHEIS As String = "&&" + DeveloperId + "##" + DeveloperPassword + "&&"
            Dim MySignatureHEIS As String = MyFunction.GetEncryption(b4hashstringHEIS)

            Dim SeqTime As String = DateTime.Now.ToString("ddMMyyyyHHmm")

            Dim SubscribeToReq As New HEIS.SubscribeNewHEISReq
            Dim SubscribeToResp As New HEIS.SubscribeNewHEISResp

            Dim oRSItem As SqlDataReader
            Dim cmdList As New SqlCommand("JumboJuniorCampaignGetList", strConn)
            cmdList.CommandType = CommandType.StoredProcedure

            oRSItem = cmdList.ExecuteReader

            If oRSItem.HasRows Then

                Do While oRSItem.Read()

                    ProcessStatus = ""

                    TotalRecord = TotalRecord + 1

                    SubscribeToReq.DeveloperID = DeveloperId
                    SubscribeToReq.OrderType = 100
                    SubscribeToReq.UserName = oRSItem("subscriberID")

                    If oRSItem("subscriberID").Contains("@iptv") = True Then
                        SubscribeToReq.ProductID = "214"
                    Else
                        SubscribeToReq.ProductID = "INT11111"
                    End If

                    SubscribeToReq.SequenceID = "JJC" & SeqTime & TotalRecord
                    SubscribeToReq.Signature = MySignatureHEIS
                    SubscribeToResp = WS_HEIS.Subscription_New(SubscribeToReq)

                    If SubscribeToResp.ResponseCode = "150100" Then
                        Status = 6          'VALID
                        ProcessStatus = "Successful Add JJ product"

                    ElseIf SubscribeToResp.ResponseCode = "150107" Then
                        Status = 7          'INVALID
                        ProcessStatus = "Subscriber ID does NOT exist"
                    Else
                        Status = 10         'PENDING
                        ProcessStatus = "Exception Add JJ product (" & DateTime.Now.ToString("dd-MM-yyyy") & ") - " & SubscribeToResp.ResponseMsg & "(" & SubscribeToResp.ResponseCode & ")"
                    End If

                    Dim cmdUpdate As New SqlCommand("JumboJuniorCampaignUpdate", strConn)
                    cmdUpdate.CommandType = CommandType.StoredProcedure
                    cmdUpdate.Parameters.Add("@SubscriberID", SqlDbType.VarChar, 50).Value = oRSItem("subscriberID")
                    cmdUpdate.Parameters.Add("@Status", SqlDbType.Int).Value = Status
                    cmdUpdate.Parameters.Add("@SequenceID", SqlDbType.VarChar, 30).Value = SubscribeToReq.SequenceID
                    cmdUpdate.Parameters.Add("@ResponseCode", SqlDbType.VarChar, 50).Value = SubscribeToResp.ResponseCode
                    cmdUpdate.Parameters.Add("@ProcessStatus", SqlDbType.VarChar, -1).Value = ProcessStatus

                    cmdUpdate.ExecuteNonQuery()
                Loop
            Else
                'NO Data - Do nothing
            End If

            oRSItem.Close()

        Catch ex As Exception
            'Do Nothing
        Finally
            If strConn.State = ConnectionState.Open Then
                strConn.Close()
            End If
        End Try

        LiteralRequest.Text = TotalRecord

        'Dim RequestString As String = myFunc.GetSerializeXML(MyRequest, MyRequest.GetType)
        'LiteralRequest.Text = Server.HtmlEncode(RequestString)
        'LiteralResponse.Text = Server.HtmlEncode(MyResponse.ResponseMessage)
    End Sub
End Class
